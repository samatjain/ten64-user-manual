---
layout: page
title: OpenWrt on Ten64
permalink: /software/openwrt/
---

Our OpenWrt repository can be found at [gitlab.com](https://gitlab.com/traversetech/ls1088firmware/openwrt). Binaries can be downloaded
from our [archive server](https://archive.traverse.com.au/pub/traverse/ls1088firmware/openwrt/). 

Until the board is upstreamed into OpenWrt, development is done by periodically rebasing on the latest OpenWrt development tree - please check
which one is the most recent before downloading.

There are two flavours to choose from in the OpenWrt build system (under 'NXP Layerscape -> ARMv8 Based Boards')

1. ```traverse-ls1088``` is for images that boot from block storage (NVMe, USB drives and SD cards).

    This generates an image that will work with the U-Boot distroboot process.

2. ```traverse-ls1088-mtd``` is for images that boot from raw flash devices (e.g SPI-NAND).

    The kernel in this image is a FIT binary containing the device tree.

## Installing OpenWrt to NAND

Our build process will generate binaries for both variants listed above.

To install OpenWrt into a NAND partition from the [recovery](/software/recovery/) system:

1. Check your MTD layout

Your U-Boot and Recovery need to implement the [dual-ubi partition scheme](/hardwre/flash/), which is the default starting September 2020.


In U-Boot

In recovery:
    cat /proc/mtd

    dev:    size   erasesize  name
    mtd0: 00100000 00001000 "bl2"
    ....
    mtd9: 06c00000 00020000 "ubia"
    mtd10: 06c00000 00020000 "ubib"

2. Download the ubifs binary

The binary for NAND has the format ```openwrt-layerscape-armv8_64b-traverse_ls1088-```__```mtd-ubifs-nand.ubi```__

3. Write the binary to the ubifs partition

```
root@recovery000afa242400:/tmp# ubiformat /dev/mtd9 -f openwrt-layerscape-armv8_64b-traverse_ls1088-mtd-ubifs-nand.ubi
```

```
ubiformat: mtd9 (nand), size 113246208 bytes (108.0 MiB), 864 eraseblocks of 131072 bytes (128.0 KiB), min. I/O size 2048 bytes
libscan: scanning eraseblock 863 -- 100 % complete
ubiformat: 863 eraseblocks have valid erase counter, mean value is 6
ubiformat: 1 bad eraseblocks found, numbers: 819
ubiformat: flashing eraseblock 247 -- 100 % complete
ubiformat: formatting eraseblock 863 -- 100 % complete
```