---
layout: page
title: DDR
permalink: /hardware/ddr/
---

The Ten64 board supports DDR4 SODIMM's with (and without) ECC running at 2100MT/sec (faster sticks are generally backwards-compatible).

The LS1088 DDR controller is generally designed for embedded use cases, hence it requires data about the memory devices before hand to set up its timing and write levelling mechanisms.

Support for SO-DIMM's is based on the JEDEC PCB reference design (also called "Raw Card"). At the time of writing JEDEC has released designs A through to K, but only a few are used in practice (especially for speeds >=2666MT/s). Most reference designs have more than one version, as higher DDR4 speeds require tighter timing margins.

Generally Traverse stocks [Innodisk](https://www.innodisk.com/en/products/dram-module/server/Unbuffered_w_ECC_DDR4_SODIMM) brand memory, but we will implement support for other vendors and models on request. 
Ten64's purchased in the crowdfunding campaign came furnished with Kingston ECC modules.

If you are looking for an ECC SO-DIMM to purchase from retail distributors, we recommend __Kingston__ as these are easy to find and tend not to have compatibility issues.

## List of tested SO-DIMM's:
|Manufacturer | Model                  | Size | ECC | Ref Design | Advertised Speed | Supported | Comments / Datasheet (if any)                                                                                                                                                 |
|-------------|------------------------|------|-----|------------|------------------|-----------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Innodisk    | M4D0-4GSSPCIK          | 4GB  | YES | D1         | 2666             | YES       | |
| Innodisk    | M4D0-8GS1PCRG          | 8GB  | YES | D1         | 2133             | YES       | |
| Innodisk    | M4D0-8GS1ICIK          | 8GB  | YES | D2         | 2666             | YES       | This is a ["very low profile"](https://www.innodisk.com/en/products/dram-module/Embedded/embedded_unbuffered_w_ecc_ddr4_sodimm_vlp) SO-DIMM suitable for height-constrained applications |
| Innodisk    | M4D0-AGS1QCRG          | 16GB | YES | G1         | 2133             | YES       | |
| Innodisk    | M4D0-BGS2QCIK          | 32GB | YES | G1         | 2666             | YES       | |
| Kingston    | KVR24S17S6/4           | 4GB  | NO  | C          | 2400             | YES       | [Kingston Website](https://www.kingston.com/dataSheets/KVR24S17S6_4.pdf)                                                                                                                          |
| Kingston    | KSM26SES8/8HD          | 8GB  | YES | D1         | 2666             | YES       | [Kingston Website](https://www.kingston.com/dataSheets/KSM26SES8_8HD.pdf)    |
| Kingston    | KSM26SES8/16ME         | 16GB | YES | D1         | 2666             | YES       | [Kingston Website](https://www.kingston.com/dataSheets/KSM26SES8_16ME.pdf)                                                                                                    |
| Kingston    | KSM26SED8/16HD         | 16GB | YES | G1         | 2666             | YES       | [Kingston Website](https://www.kingston.com/dataSheets/KSM26SED8_16HD.pdf)  |
| Kingston    | KSM26SED8/32ME         | 32GB | YES | G1         | 2666             | YES       | [Kingston Website](https://www.kingston.com/dataSheets/KSM26SED8_32ME.pdf)                                                                                                    |
| Kingston    | KSM29SED8/32ME<br/>KTL-TN429E/32G | 32GB | YES | G1 | 2933          | YES       | [Kingston Website](https://www.kingston.com/dataSheets/KSM29SED8_32ME.pdf). [KTL-TN429E/32G](https://www.kingston.com/en/memory/search?partId=KTL-TN429E%2F32G) is an identical part intended as a substitute for Lenovo systems |
| Micron      | MTA18ASF2G72HZ-2G6E1ZK | 16GB | YES | G1         | 2666             | YES       | Also known as Crucial CT16G4TFD8266. Datasheet on micron.com (login required), previous model is ref card H not tested                                                        |
| Crucial     | CT4G4SFS8266.C8FE      | 4GB  | NO  | A2         |2666              | YES       | [Crucial Website](https://www.crucial.com/usa/en/ct4g4sfs8266), does not have a corresponding Micron OEM version. SPD contains incorrect DQ byte mappings, fixed in firmware. (Tsk tsk Crucial!) |
| Crucial     | CT8G4SFS824A.C8FN      | 8GB  | NO  | A2         | 2400             | YES       | [Crucial Website](https://www.crucial.com/memory/ddr4/ct8g4sfs824a)  |
| Crucial     | CT16G4SFD824A.C16FDD1  | 16GB | NO  | B1         | 2400             | YES       | [Crucial Website](https://www.crucial.com/memory/ddr4/ct16g4sfd824a) |
| Crucial     | CT32G4SFD8266.C16FB    | 32GB | NO  | E1         | 2666             | YES       | [Crucial Website](https://www.crucial.com/memory/ddr4/ct32g4sfd8266) |
| Samsung     | M471A4G43MB1-CTD       | 32GB | NO  | E1         | 2666             | YES       | [Samsung Website](https://www.samsung.com/semiconductor/dram/module/M471A4G43MB1-CTD/)                                                                                                           |


__Tip__: The Ref card version can be found in the standard JEDEC model ID that is on the label of most cards, for example:

* Samsung M471A4G43MB1-CTD: PC4-2666V-S __E1__ -11
* Micron MTA18ASF2G72HZ-2G6E1ZK: PC4-2666V-T __G1__ -11

When BL2/TF is compiled in debug mode, the raw card parameter will be printed out at boot:
<pre>
INFO:    RCW BOOT SRC is QSPI
INFO:    RCW BOOT SRC is QSPI
WARNING: Using I2C3 for DDR SPD
INFO:    platform clock 700000000
INFO:    DDR PLL 2100000000
INFO:    time base 11 ms
INFO:    Parse DIMM SPD(s)
....
INFO:    n_ranks 2
INFO:    rank_density 0x400000000
INFO:    capacity 0x800000000
INFO:    die density 0x6
INFO:    primary_sdram_width 64
INFO:    ec_sdram_width 0
INFO:    device_width 8
INFO:    package_3ds 0
<b>INFO:    rc 0x24 (Raw card E1)</b>
INFO:    rdimm 0
INFO:    mirrored_dimm 1
INFO:    n_row_addr 17
INFO:    n_col_addr 10
</pre>

Despite support for most reference designs being implemented, we have found quite a few memory modules ship with incorrect data in their EEPROM (SPD), requiring workarounds in our memory init code. If an unimplemented SO-DIMM fails to boot despite support for the reference design in BL2, we can usually determine the cause of the problem from the full (debug) boot log.

## Adding support for new SO-DIMM's / raw cards
The following parameters are needed to support a SO-DIMM card:

* JEDEC raw card ID
* Clock adjust and base write level values (for the first byte lane)
* Write level values for the remaining byte lanes

The clock adjust and write level values are found via NXP's CodeWarrior [QCVS DDR](https://www.nxp.com/docs/en/user-guide/QCVS_DDR_User_Guide.pdf) tool - which determines the most stable values by iteration on the actual hardware. In theory, you could try and determine these intuitively based on the DIMM net lengths - but we don't recommend this!

These are put into a table in the Trusted Firmware [ddr_init.c](https://gitlab.com/traversetech/ls1088firmware/arm-trusted-firmware/blob/ten64/plat/nxp/soc-ls1088/ten64/ddr_init.c#L62).
