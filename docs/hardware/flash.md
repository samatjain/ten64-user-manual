---
layout: page
title: Flash
permalink: /hardware/flash/
---

# Flash Layout
Ten64 boards have two flash IC's, which are used to house the low-level boot firmware
and other useful utilities (such as the [recovery](/recovery) image):

* A 8MB (64Mbit) SPI NOR flash - houses ARM Trusted Firmware, U-Boot, DPAA2 FW, DPAA2 config files

* A 256MB (2Gbit) SPI-NAND flash - contains [recovery](/recovery) and OpenWrt (two separate partitions to support A/B upgrades)

Future variants (starting with B2B/OEM variants first) may remove the 8MB NOR flash in favour of having NAND only - the first 8MB on the NAND
has been reserved to facilitate this.

## NOR layout
| Firmware Definition                        | MaxSize (KiB) | Flash Offset | Hex offset | Hex size |
|--------------------------------------------|---------------|--------------|------------|----------|
| RCW + PBI + BL2 (bl2.pbl)                  | 1048576       | 0            | 0          | 100000   |
| ATF FIP Image (fip.bin) BL31 + BL32 + BL33 | 2097152       | 1048576      | 100000     | 200000   |
| DPAA2 MC                                   | 2097152       | 3145728      | 300000     | 200000   |
| Boot firmware environment                  | 524288        | 5242880      | 500000     | 80000    |
| DPAA2 DPL                                  | 262144        | 5767168      | 580000     | 40000    |
| DPAA2 DPC                                  | 262144        | 6029312      | 5C0000     | 40000    |
| Device Tree                                | 262144        | 6291456      | 600000     | 40000    |

## NAND layout
| Firmware Definition              | MaxSize (KiB) | Flash Offset | Hex offset | Hex size |
|----------------------------------|---------------|--------------|------------|----------|
| Reserved (future boot from NAND) | 8388608       | 0            | 0          | 800000   |
| Recovery                         | 33554432      | 8388608      | 800000     | 2000000  |
| OpenWrt ubifs (slot A)           | 113246208     | 41943040     | 2800000    | 6C00000  |
| OpenWrt ubifs (slot B)           | 113246208     | 155189248    | 9400000    | 6C00000  |

## mtdparts and kernel mtd output:
```
# mtdparts on u-boot environment and kernel command line
mtdparts=mtdparts=spi-nand0:8m(reserved),32m(recovery),108m(ubia),108m(ubib);nor1:1m(bl2),2m(bl3),2m(mcfirmware),512k(ubootenv),256k(dpl),256k(dpc),256k(devicetree)

# /proc/mtd:
mtd0: 00100000 00001000 "bl2"
mtd1: 00200000 00001000 "bl3"
mtd2: 00200000 00001000 "mcfirmware"
mtd3: 00080000 00001000 "ubootenv"
mtd4: 00040000 00001000 "dpl"
mtd5: 00040000 00001000 "dpc"
mtd6: 00040000 00001000 "devicetree"
mtd7: 00800000 00020000 "nand-boot-reserved"
mtd8: 02000000 00020000 "recovery"
mtd9: 06c00000 00020000 "ubia"
mtd10: 06c00000 00020000 "ubib"
```
