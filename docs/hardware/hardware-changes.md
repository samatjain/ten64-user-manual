# Hardware Changes and Errata

### Errata
| Affected board revisions | Issue                                                                                              | Impact                                                  | Workarounds                                                 | Proposed Resolution                                                                                                                |
|--------------------------|----------------------------------------------------------------------------------------------------|---------------------------------------------------------|-------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------|
| Ten64 Rev B,C      | RTC (Epson RX-8035) interrupt is not functional due to inversion/missing pullup                    | Low (no effect found on any Linux distribution to date) | Ensure there is no IRQ reference for the RTC in device tree | This will be fixed in a future Ten64 board respin.                                                                                 |
| Ten64 Rev B,C            | It may not be possible to install an ATX (UNC) mounting screw for the mounting hole next to the fan connector | Low                                                     | Use a smaller mounting screw (e.g M3) or do not install     | A future Ten64 revision may reposition the fan connector, the current position was a compromise due to layout requirements |


## From Ten64 Rev B to Ten64 Rev C (first production version)
* Incorporation of 'wire mod' rework fixes
* Improve clearances around mounting holes and power connector
* De-feature I2C4 access to M.2 cards due to this not being adhered to by some Key B modems. It can restored by adding the missing parts.
* Add I2C control to USB Hub, to allow the USB3 lanes to be turned off when a PCIe only card (e.g SSD) is installed in Key B (fixes "cannot connect" warnings in the kernel)
* Adjusted heights of M.2 slots to better facilitate use of 42mm cards inside the 52mm/80mm footprints.
* The microcontroller (LPC804) was redeveloped between RevB and RevC to fix I2C interoperability issues with U-Boot

## From Ten64 Rev A to Ten64 Rev B
* Incorporation of 'wire mod' rework fixes
* Re-allocate one PCIe 3.0 lane from SoC from MiniPCIe WiFi to M.2/B
* Tweak layout so mounting hole for 5G cards can be on the board.
* Add mounting hole at 42mm for M.2/M
* SIM only position on SIM tray is now allocated to SIM1 on M.2/B, SIM2 is shared with microSD or eSIM (build time option).
* The microSD pins on the board are now electrically isolated when the tray is in the eject position.
* Add 4-pin fan header for CPU fan + EMC2301 PWM controller.
* The SPI-NAND flash size was increased to 256MB (2Gbit).
* DDR SPD and RTC moved to I2C3 bus, TPM moved to I2C1. (prevents interference by other I2C1 devices in boot + allows EFI RTC protocol implementation in the future)
* Added a 'power button' line from the microcontroller to LS1088 which can be used for ACPI-style power button events.
* The single 'admin' button on the rear panel was replaced with a stacked dual-LED/button combination to better accommodate enclosures without ATX front panel LEDs/buttons.
* SFP cage was reduced from four LEDs to two LEDs (one for each slot).
