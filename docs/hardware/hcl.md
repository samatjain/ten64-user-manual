# Hardware Compatibility List

This page lists hardware that we (Traverse) test and use with our boards on a
regular basis.

Standard PCIe/USB3/LTE/SFP modules should work, we advise that you test
any third-party modules aggressively before starting any mass deployments.

## DDR SO-DIMMs
See the [DDR](/hardware/ddr) page

## Storage

### M.2 NVMe
<strong>Only PCI Express (e.g NVMe) devices will work in the M.2 Key M (SSD slot)</strong>

There are only two PCIe lanes going to this slot, so high end SSDs with large DRAM caches
(designed to saturate a PCIe3 x4 connection) may not provide much of a performance benefit[^1].

NVMe drives with key B and in the M.2 22x42mm form factor such as the Western Digital SN520 P/N SDAPMUW-128G-1022 will also work in the "cellular" (key B)
slot.

A sample of known working devices: (any NVMe drive should work)

* WD Blue SN500, SN550 (2019 version - the older versions are SATA)
* Crucial P1
* Samsung 960 PRO
* Apacer AS228AP2 series (including 85.DCD650.B011C)

Some PCIe 3.0 drives such as the Patriot P300 tend to link up at PCIe 2.0 speeds but are otherwise
function correctly, these issues are to be investigated further.

### USB

Some older USB2 era drives are known to cause boot stalls in U-Boot as they don't finish initializing by the time U-Boot does a
USB scan (this occurs much faster [relative to ```t=0```] than on PCs of the comparable era).

If you encounter issues with this, you can adjust the ```bootdelay``` and/or ```bootmenu_delay``` in U-Boot.

Sandisk's UltraFit range (model number SDCZ430) works nicely as a 'boot' drive in the internal USB connector for OpenWrt, uVirt and other lightweight distributions.

### SATA/RAID controllers
* InnoDisk EMPS-3401-C1 mPCIe to 4xSATA3.0 (Marvell chipset)
* InnoDisk EGPS-3401-C1 M.2 B/M to 4xSATA3.0 (Marvell chipset)
* IOCrest IO-M2F585-5I M.2 B/M (80mm) to 5xSATA3 (JMicron chipset)

## WiFi
Most ath9k and ath10k cards should work - see the [ath10k wiki](https://wireless.wiki.kernel.org/en/users/drivers/ath10k) for a list of hardware.

* Compex WLE200N2 802.11n (ath9k)
* Compex WLE600VX 802.11ac 2x2 (ath10k)
* Compex WLE1216V5-20 802.11ac Wave 2 4x4 (ath10k)
* jjPlus JWX6058 802.11ac 2x2 + BT (ath10k) - must be in the mPCIe slot nearest the M.2/M for BT to work
* jjPlus JWX6052 802.11ac 3x3 (ath10k)

Note that Intel WiFi cards (AX200 etc.) do not support access point mode (except for some P2P/WiFi direct use cases).

## LTE and 5G
All M.2 modem cards should work - beware that the dual SIM feature is designed to work with cards that have
two sets of SIM pins - there is no SIM switcher on the Ten64 board. See the [LTE](/lte/) page for more details.

### LTE

* Sierra EM7430/EM7455 Cat6
* Quectel EM12 Cat12

### 5G
5G cards in the 30x52mm form factor are supported, so far these have been tested (or reported by users) to work:

* SIMCOM SIM8200EA
* Fibocom FM150[^2]
* Quectel RM500Q
* The Telit FN980m also works, but does not mount mechanically because it is slightly shorter and thicker than the others.

Cards which DO NOT work:

* Sierra EM919x and EM7690: require a non-standard VBUS_SENSE pin.

## SFP and SFP+ transceivers

### 10 Gigabit
Practically any 10G SFP (excluding 10GBASE-LRM, see below) should work in the Ten64.

* Passive SFP+ cables:
    * Cisco SFP-H10GB-CU3M
    * Ubiquiti Networks 1 and 2m SFP+
    * FiberStore SFP+
* Ubiquiti Networks "UFiber" 10G MultiMode (UF-MM-10G)
* Finisar FTLX8571D3BCL 10GBASE-SR (Multimode)
* FiberStore/fs.com 10GBASE-SR (Multimode)
* FiberStore/fs.com SFP-10GLR-31 10GBASE-LR (Single mode)
*	FiberStore/fs.com 10G SFP+ Active Optical [Direct-attach] cable
* Mikrotik S+RJ10 10GBASE-T SFP+

    Due to the thermal characteristics of these modules (they get VERY hot), it is not recommended to have two of them
    installed at the same time, one or both of the modules may fail to link with the host due to the effect
    on the signal.

    A workaround for this is to use an SFP extender cable, such as [CVT-S/S-Cage](https://www.sfpcables.com/sfp-to-sfp-cage-with-3m-flat-cable-in-nylon-jacket-20cm-and-55cm-length-3256) to locate one or both
    10GBASE-T modules away from each other.

10GBASE-LRM (Long Range [Legacy] Multimode) transceivers are not supported - these require additional signal conditioning capabilities than the TI retimer can provide.

### 1 Gigabit
* 1000BASE-SX (850nm over MMF):
    * Ubiquiti Networks "UFiber" 1G MultiMode (UF-MM-1G)
* 1000BASE-T
    * Ubiquiti Networks UF-RJ45-1G
    * FiberStore/fs.com SFP-GB-GE-T
* VDSL 17a
    * Proscend 180-T

NOT Compatible: Mikrotik S-85DLC05D due to invalid EEPROM contents - it fails
various checks in the Linux phylink/SFP attach code.


## Mini-ITX cases and power supplies
As noted in [power connector](/hardware/power-connector/), some older ATX power supplies will not work with no load on the 3.3V and 5V rails.

Cases and power supplies we know to work:

* In-Win IW-MS04-1 Mini-Server/NAS chassis 
* In-Win BQ656S (marketed as "Chopin Black") chassis with In-Win 150W OEM PSU
* In-Win B1
    * Tip: The SATA data cables for the 2.5" drive bays inside this case are quite short and will not reach a SATA controller installed in any of the card slots - you will want an extender cable, such as [StarTech SATAEXT30CM](https://www.startech.com/en-us/cables/sataext30cm).
* SeaSonic SS-250SU Flex ATX PSU
* Delta Electronics DPS150 and DPS250 Flex ATX PSU

Not compatible:

* U-NAS NSC200/201/400 have a mechanical interference issue between the mounting mechanism and the power connector on RevB boards - we are investigating changes in the layout for RevC
* Foxconn FSP150-50GLT power supply (powers down on 3.3V/5V no load)

[^1]: The LS1088 only has four lanes of PCIe - which is split x2 to M.2/M, x1 to PCIe Switch (then miniPCIe) and x1 to M.2/B, hence giving a full x4 connection isn't possible in our configuration.
[^2]: Very early samples of the Fibocom card required an external mode switch signal to select between PCIe and USB mode. This has now been resolved in recent versions - if you do not have PCIe drivers you will need to use an AT command to select USB mode.