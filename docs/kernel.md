# Kernel for NXP LS1088

We maintain a patchset for our boards at [gitlab.com/traversetech/traverse-kernel-patches](https://gitlab.com/traversetech/traverse-kernel-patches). These are used to build our customized distribution images.

There are multiple versions supported, each as individual branches in the repository,
such as ``torvalds`` for the current mainline and ``lts-419`` for the 4.19 Long Term Stable kernel.

## Upstream status
* Kernel 5.0 and later will boot on the LS1088 out of the box with PCIe and networking functionality.

* 5.1 and later needs the attribute ``arm-smmu.disable_bypass=n`` on the [kernel command line](/software/distributions/#kernel-51-and-later-iommu-bypass-fault-on-boot) - pending a fix to the DPAA2 IOMMU handling.

    Some distributions, such as OpenSuSE disable the bypass in their Kconfig. We are investigating
if this option could be forced by the bootloader as well.

    <strong>Update for 5.5 and later</strong>

    We have found issues getting the ``arm.smmu.disable_bypass`` recognized by kernel 5.5 and later - we are now disabling it in Kconfig for our own kernel packages.

We also maintain some sensor drivers out of tree at [https://gitlab.com/traversetech/ls1088firmware/traverse-sensors](https://gitlab.com/traversetech/ls1088firmware/traverse-sensors).

## Required kernel options
You will need:
```
## file: arch/arm64/Kconfig
# The USB3 host (DWC3) drive does not like
# 39-bit virtual address space
# To our knowledge, all distributions are using
# 48-bit anyway.
CONFIG_ARM64_VA_BITS_48=y
CONFIG_ARM64_VA_BITS=48

## file: arch/arm64/Kconfig.platforms
CONFIG_ARCH_LAYERSCAPE=y

## file: drivers/bus/fsl-mc/Kconfig
CONFIG_FSL_MC_BUS=y

## drivers/char/hw_random/Kconfig
CONFIG_CRYPTO_HW=y

## drivers/crypto/caam/Kconfig
CONFIG_CRYPTO_DEV_FSL_CAAM_COMMON=y
CONFIG_CRYPTO_DEV_FSL_CAAM_CRYPTO_API_DESC=y
CONFIG_CRYPTO_DEV_FSL_CAAM_AHASH_API_DESC=y
CONFIG_CRYPTO_DEV_FSL_CAAM=m
# CONFIG_CRYPTO_DEV_FSL_CAAM_DEBUG is not set
CONFIG_CRYPTO_DEV_FSL_CAAM_JR=m
CONFIG_CRYPTO_DEV_FSL_CAAM_RINGSIZE=9
# CONFIG_CRYPTO_DEV_FSL_CAAM_INTC is not set
CONFIG_CRYPTO_DEV_FSL_CAAM_CRYPTO_API=y
CONFIG_CRYPTO_DEV_FSL_CAAM_CRYPTO_API_QI=y
CONFIG_CRYPTO_DEV_FSL_CAAM_AHASH_API=y
CONFIG_CRYPTO_DEV_FSL_CAAM_PKC_API=y
CONFIG_CRYPTO_DEV_FSL_CAAM_RNG_API=y
CONFIG_CRYPTO_DEV_FSL_DPAA2_CAAM=y

## drivers/gpio/Kconfig
CONFIG_GPIO_MPC8XXX=y

## drivers/net/ethernet/freescale/Kconfig
CONFIG_NET_VENDOR_FREESCALE=y
CONFIG_FSL_PQ_MDIO=y
CONFIG_FSL_DPAA_ETH=m

## drivers/net/ethernet/freescale/dpaa2/Kconfig
CONFIG_FSL_DPAA2_ETH=m
CONFIG_FSL_DPAA2_PTP_CLOCk=m
# CONFIG_GIANFAR is not set
# CONFIG_FSL_ENETC is not set

## drivers/pci/controller/dwc/Kconfig
CONFIG_PCI_LAYERSCAPE=y
CONFIG_PCIE_LAYERSCAPE=y

## drivers/soc/fsl/Kconfig
CONFIG_FSL_MC_DPIO=y
```

For kernel versions with the IOMMU Bypass<->DPAA2 incompatibility, you will need:
```
## drivers/iommu/Kconfig
# CONFIG_ARM_SMMU_DISABLE_BYPASS_BY_DEFAULT is not set
```
